![](https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=2ahUKEwjp6YPbh6nkAhU8HbkGHTZ-C_UQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.shutterstock.com%2Fsearch%2Fimage&psig=AOvVaw2_jYYIiqwJYBdyaQSXnqmb&ust=1567201775586629)

# JPW - 2019 UNESC
Disciplina de Java para Web
Professor Ramon Venson

Exercícios 25
Crie um fork do repositório implemente as alterações nas funções dapasta API. Proponha um merge request para as funções implementadas.

* api/aleatorio.js
* api/all.js
* api/balanceados.js
* api/fragil.js
* api/johto_pokedex.js
* api/kanto_pokedex.js
* api/lentos.js
* api/letras.js
* api/rapidos.js
* api/soma_ataques_sp.js
* api/soma_ataques.js
* api/soma_defesas_sp.js
* api/soma_defesas.js
* api/sprites.js
* api/tanks.js
* api/tipo_agua.js
* api/tipo_duplo.js
* api/tipo_fogo.js
* api/tipo_grama.js
* api/ubers.js